from random import randint

game = input("Python will guess your birthday!")
name = input("What is your name?")

for guess_number in range(1, 6):
    month_number = randint(1, 13)
    year_number = randint(1000, 2023)

    print("Guess", guess_number, ":", name, "were you born in", month_number, "/", year_number, "?")

    answer = input("Yes or No?")
    
    if answer == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have better things to do. Good Bye!")
    else:
        print("Drat! Lemme try again!")
